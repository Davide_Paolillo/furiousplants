﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class MouseSpawner : MonoBehaviour
{
    private GameObject defenderToSpawn;
    private DefenderButton[] defenders;
    private ResourcesHandler resourcesHandler;

    private List<Vector2> mousePosMap;
    private Vector2 mousePos;

    public Vector2 DefenderPosition { get => mousePos; }

    private void Start()
    {
        resourcesHandler = GameObject.FindObjectOfType<ResourcesHandler>();
        mousePosMap = new List<Vector2>();
        SetDefenderToSpawn();
    }

    private void SetDefenderToSpawn()
    {
        defenders = FindObjectsOfType<DefenderButton>();
        foreach (DefenderButton defender in defenders)
        {
            if (defender.IsActive)
            {
                defenderToSpawn = defender.ObjToSpawn;
            }
        }
    }

    void Update()
    {
        SetDefenderToSpawn();
        if (Input.GetMouseButtonDown(0) && defenderToSpawn != null && isResEnough())
        {
            CheckClikOnGameGrid();
        }
    }

    private void CheckClikOnGameGrid()
    {
        var hitGameGrid = Physics2D.Raycast(Camera.main.ScreenToWorldPoint(Input.mousePosition), Vector2.zero);
        if (hitGameGrid.collider != null && hitGameGrid.collider.gameObject.tag.Equals(GlobalVar.GAME_AREA_TAG))
        {
            var mouseWorldPoint = Camera.main.ScreenToWorldPoint(Input.mousePosition);
            var defenderZPos = defenderToSpawn.transform.position.z;
            mousePos = new Vector2(mouseWorldPoint.x,mouseWorldPoint.y);
            mousePos = SnapToGrid(mousePos);
            if (isGridEmpty(mousePos))
            {
                InstantiateNewDefender(mousePos);
            }
        }
    }

    private bool isResEnough()
    {
        if (resourcesHandler.Resources > 0)
        {
            return resourcesHandler.Resources - defenderToSpawn.GetComponent<Defender>().ResourcesCost >= 0 ? true : false;
        }
        else
        {
            return false;
        }
    }

    private void InstantiateNewDefender(Vector2 mousePos)
    {
        GameObject defender = Instantiate(defenderToSpawn, mousePos, Quaternion.identity);
        defender.transform.parent = FindObjectOfType<MouseSpawner>().transform; // check if works when destroyed
        var resToSubtract = -(defenderToSpawn.GetComponent<Defender>().ResourcesCost);
        resourcesHandler.UpdateResources(resToSubtract);
    }

    private bool isGridEmpty(Vector2 mousePos)
    {
        if (mousePosMap.Count > 0)
        {
            foreach (Vector2 pos in mousePosMap)
            {
                if (mousePos == pos)
                {
                    return false;
                }
            }
            mousePosMap.Add(mousePos);
            return true;
        }
        else
        {
            mousePosMap.Add(mousePos);
            return true;
        }
    }

    public void RemovePositionFromGridMapWahenDestroyed(Vector2 myPos)
    {
        foreach (Vector2 pos in mousePosMap)
        {
            if (pos == myPos)
            {
                mousePosMap.Remove(pos);
                return;
            }
        }
    }

    private Vector2 SnapToGrid(Vector2 mousePos)
    {
        mousePos.x = Mathf.RoundToInt(mousePos.x);
        mousePos.y = Mathf.RoundToInt(mousePos.y - 0.4f) + 0.5f;
        return mousePos;
    }
}
