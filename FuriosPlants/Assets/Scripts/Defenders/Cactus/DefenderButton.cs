﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DefenderButton : MonoBehaviour
{
    [SerializeField] private GameObject objToSpawn;

    private Color disabledButtonColor;
    private bool isActive;

    public bool IsActive { get => isActive; set => isActive = value; }
    public GameObject ObjToSpawn { get => objToSpawn; }

    void Start()
    {
        disabledButtonColor = GetComponent<SpriteRenderer>().color;
        isActive = false;
    }

    private void OnMouseDown()
    {
        var otherButtons = FindObjectsOfType<DefenderButton>();
        foreach (DefenderButton button in otherButtons)
        {
            button.GetComponent<SpriteRenderer>().color = disabledButtonColor;
            button.isActive = false;
        }
        GetComponent<SpriteRenderer>().color = Color.white;
        isActive = true;
    }

    
}
