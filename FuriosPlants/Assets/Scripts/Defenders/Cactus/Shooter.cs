﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Shooter : MonoBehaviour
{
    [SerializeField] private GameObject projectile;
    [SerializeField] private GameObject gun;

    private List<GameObject> spawners;
    private PositionRounder positionRounder;
    private Animator animator;

    private void Start()
    {
        animator = GetComponent<Animator>();
        positionRounder = new PositionRounder();
        spawners = new List<GameObject>();
        SetLaneSpawner();
    }

    private void SetLaneSpawner()
    {
        var spawnersInScene = GameObject.FindGameObjectsWithTag(GlobalVar.SPAWNER_TAG);
        foreach (GameObject spawner in spawnersInScene)
        {
            spawners.Add(spawner);
        }
    }

    private void Update()
    {
        if (AttackerIsInLane())
        {
            animator.SetBool(GlobalVar.IS_ATTACKING_ANIMATION, true);
        }
        else
        {
            animator.SetBool(GlobalVar.IS_ATTACKING_ANIMATION, false);
        }
    }

    private bool AttackerIsInLane()
    {
        var myPosition = GetComponent<Defender>().MyRoundedYPosition;
        foreach (GameObject spawner in spawners)
        {
            var roundedYSpawnerPos = positionRounder.RoundedYPosition(spawner.transform.position);
            if (myPosition.y == roundedYSpawnerPos.y)
            {
                if (spawner.transform.childCount > 0)
                {
                    return true;
                }
                else
                {
                    return false;
                }
            }
        }
        return false;
    }

    public void Fire()
    {
        GameObject bulletsPool = GameObject.FindGameObjectWithTag(GlobalVar.BULLETS_POOL_TAG);
        GameObject bullet = Instantiate(projectile, gun.transform.position, gun.transform.rotation) as GameObject;
        bullet.transform.parent = bulletsPool.transform;
    }

    public void FireDelayed(float delay)
    {
        StartCoroutine(FireBullet(delay));
    }

    private IEnumerator FireBullet(float delay)
    {
        yield return new WaitForSeconds(delay);
        Fire();
    }
}
