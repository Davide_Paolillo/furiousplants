﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class DamageDealer : MonoBehaviour
{
    [SerializeField] private float damageDealt = 10.0f;

    public float DamageDealt { get => damageDealt; }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.LIZARD_TAG) || collision.gameObject.tag.Equals(GlobalVar.ENEMY_TAG))
        {
            Destroy(this.gameObject);
        }
        else if (collision.gameObject.tag.Equals(GlobalVar.PROJECTILE_COLLIDER_TAG))
        {
            Destroy(this.gameObject);
        }
    }
}
