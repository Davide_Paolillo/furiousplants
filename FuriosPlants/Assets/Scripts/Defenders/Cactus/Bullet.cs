﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Bullet : MonoBehaviour
{
    [SerializeField] private float bulletSpeed = 2.0f;

    void Update ()
    {
        this.transform.Translate(Vector2.right * Time.deltaTime * bulletSpeed);
    }

    public void DestroyOnDemand ()
    {
        Destroy(this.gameObject);
    }
}
