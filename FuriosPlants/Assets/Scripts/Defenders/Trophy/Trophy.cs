﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Trophy : MonoBehaviour
{
    [SerializeField] private int resToAdd = 1;

    private ResourcesHandler resHandler;

    private void Start()
    {
        resHandler = FindObjectOfType<ResourcesHandler>();
    }

    public void UpdateScoreWhenSmash()
    {
        resHandler.UpdateResources(resToAdd);
    }
}
