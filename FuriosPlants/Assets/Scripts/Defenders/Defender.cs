﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Defender : MonoBehaviour
{
    [Header("Stats")]
    [Range(1, 100)][SerializeField] private int resourcesCost = 10;
    [SerializeField] private int health = 500;

    private Vector3 myRoundedYPosition;
    private PositionRounder positionRounder;
    private Animator animator;
    private MouseSpawner mouseSpawner;

    private int prevHealth;

    public int ResourcesCost { get => resourcesCost; }
    public Vector3 MyRoundedYPosition { get => myRoundedYPosition; }
    public int Health { get => health; }

    private void Start()
    {
        mouseSpawner = GameObject.FindObjectOfType<MouseSpawner>();
        animator = GetComponent<Animator>();
        prevHealth = health;
        positionRounder = new PositionRounder();
        SetMyInitialRoundedYPos();
    }

    private void Update()
    {
        CheckHealth();
    }

    private void CheckHealth()
    {
        PlayGatDamageAnimationWhenHitted();
        if (health <= 0)
        {
            mouseSpawner.RemovePositionFromGridMapWahenDestroyed(this.gameObject.transform.position);
            Destroy(this.gameObject);
        }
    }

    private void PlayGatDamageAnimationWhenHitted()
    {
        if (prevHealth > health)
        {
            animator.SetBool(GlobalVar.IS_GETTING_DAMAGE_ANIMATION, true);
            prevHealth = health;
        }
        else
        {
            animator.SetBool(GlobalVar.IS_GETTING_DAMAGE_ANIMATION, false);
        }
    }

    private void SetMyInitialRoundedYPos()
    {
        myRoundedYPosition = positionRounder.RoundedYPosition(this.transform.position);
    }

    public void UpdateHealth(int addend)
    {
        this.health += addend;
    }
}
