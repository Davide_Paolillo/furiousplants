﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AttackerSpawner : MonoBehaviour
{
    [SerializeField] private List<GameObject> attackers;
    [SerializeField] private float minTime = 1.0f;
    [SerializeField] private float maxTime = 5.0f;

    private bool spawn = true;
    private bool isRunning;
    private float xPadding = 2.0f;

    public bool Spawn { get => spawn; set => spawn = value; }

    void Start()
    {
        isRunning = false;
    }

    private void Update()
    {
        if (spawn && !isRunning)
        {
            StartCoroutine(SpawnEnemy());
        }
        else if(!spawn)
        {
            StopAllCoroutines();
            isRunning = false;
        }
    }

    private IEnumerator SpawnEnemy()
    {
        isRunning = true;
        yield return new WaitForSeconds(UnityEngine.Random.Range(minTime, maxTime));
        SpawnAttacker();
        isRunning = false;
    }

    private void SpawnAttacker()
    {
        var attacker = GetRandomAttacker();
        GameObject newAttacker = SpawnChosenAttacker(attacker);
        newAttacker.transform.parent = this.transform;
        newAttacker.GetComponent<Attacker>().AttackDamage = DifficultyManager.SetADBasedOnDifficulty(newAttacker.GetComponent<Attacker>().AttackDamage);
    }

    private GameObject SpawnChosenAttacker(GameObject attacker)
    {
        GameObject newAttacker;
        if (attacker.gameObject.tag.Equals(GlobalVar.LIZARD_TAG))
        {
            newAttacker = Instantiate(attacker, this.transform.position, this.transform.rotation) as GameObject;
        }
        else
        {
            var fixedXPos = new Vector3(this.transform.position.x + xPadding, this.transform.position.y, this.transform.position.z);
            newAttacker = Instantiate(attacker, fixedXPos, this.transform.rotation) as GameObject;
        }

        return newAttacker;
    }

    private GameObject GetRandomAttacker()
    {
        return attackers[UnityEngine.Random.Range(0, attackers.Count)];
    }
}
