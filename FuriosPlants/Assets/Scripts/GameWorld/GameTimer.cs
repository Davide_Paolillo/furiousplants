﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class GameTimer : MonoBehaviour
{
    [Tooltip("Level timer in seconds")]
    [SerializeField] private float levelTime = 10.0f;

    private Slider mySlider;
    private List<AttackerSpawner> attackerSpawners;
    private LevelLoader levelLoader;

    private int countAttackersInScene;
    private bool timeFinished;

    public float LevelTime { get => levelTime; set => levelTime = value; }

    private void Start()
    {
        mySlider = GetComponent<Slider>();
        timeFinished = false;
        mySlider.maxValue = levelTime;
        levelLoader = FindObjectOfType<LevelLoader>();
        countAttackersInScene = FindObjectsOfType<Attacker>().Length;
        InitializeAttackerSpawnersList();
    }

    private void InitializeAttackerSpawnersList()
    {
        attackerSpawners = new List<AttackerSpawner>();
        foreach (AttackerSpawner attacker in FindObjectsOfType<AttackerSpawner>())
        {
            attackerSpawners.Add(attacker);
        }
    }

    void Update()
    {
        timeFinished = (Time.timeSinceLevelLoad >= levelTime);
        if (timeFinished)
        {
            if (!levelLoader.LvlOver)
            {
                DisableAttackerSpawners();
                CheckEnemiesAreAllDead();
            }
        }
        else
        {
            mySlider.value = Time.timeSinceLevelLoad % levelTime;
        }
    }

    private void CheckEnemiesAreAllDead()
    {
        if (FindObjectsOfType<Attacker>().Length <= 0 && FindObjectOfType<HealthHandler>().HealthPoints > 0)
        {
            levelLoader.LvlOver = true;
        }
    }

    private void DisableAttackerSpawners()
    {
        foreach (var spawner in attackerSpawners)
        {
            spawner.Spawn = false;
        }
    }

    public void SetSliderPositionToZero(float min, float max)
    {
        mySlider.minValue = min;
        mySlider.maxValue = max;
        mySlider.value = min;
    }
}
