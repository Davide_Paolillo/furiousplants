﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.SceneManagement;

public class SceneLoader : MonoBehaviour
{
    [SerializeField] private Animator smoothTransition;

    private int currentSceneIndex;

    private void Start()
    {
        currentSceneIndex = 0;
        SetPlayerPrefs();
    }

    #region PlayerPrefs
    private void SetPlayerPrefs()
    {
        SetMasterVolume();
    }

    private void SetMasterVolume()
    {
        if (PlayerPrefs.HasKey(PrefsVariables.VOLUME_SETTING))
        {
            var audioSources = FindObjectsOfType<AudioSource>();
            foreach (AudioSource audioSource in audioSources)
            {
                audioSource.volume = PrefsVariables.Volume;
            }
        }
    }
    #endregion

    public void LoadNextSceneDelayed(float loadDelay)
    {
        currentSceneIndex = SceneManager.GetActiveScene().buildIndex;
        if (currentSceneIndex < (SceneManager.sceneCountInBuildSettings - 1))
        {
            StartCoroutine(LoadSceneDelayed(currentSceneIndex + 1, loadDelay));
        }
        else
        {
            currentSceneIndex = 0;
            StartCoroutine(LoadSceneDelayed(currentSceneIndex, loadDelay));
        }
    }

    public void LoadNextScene()
    {
        LoadNextSceneDelayed(0);
    }

    public void LoadNextScene(String scene)
    {
        SceneManager.LoadScene(scene);
    }

    public void LoadMenuScene()
    {
        LoadNextScene(GlobalVar.MENU_SCENE);
    }

    public void LoadGameScene()
    {
        LoadNextScene(GlobalVar.GAME_SCENE);
    }

    public void LoadOptionsScene()
    {
        LoadNextScene(GlobalVar.OPTIONS_SCENE);
    }


    public void Quit()
    {
        Application.Quit();
    }

    private IEnumerator LoadSceneDelayed(int index, float loadDelay)
    {
        if (smoothTransition != null)
        {
            smoothTransition.SetTrigger("FadeOut");
        }
        yield return new WaitForSeconds(1.0f + loadDelay);
        SceneManager.LoadScene(index);
    }
}
