﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class LevelLoader : MonoBehaviour
{
    [Header("Fade Animators")]
    [SerializeField] private Animator defendersUIAnimator;
    [SerializeField] private Animator sliderAnimator;
    [SerializeField] private Animator winCanvasAnimator;
    [SerializeField] private float canvasAnimationDuration = 6.0f;

    [Header("SFX")]
    [SerializeField] private AudioClip audioClip;

    private GameTimer myGameTimer;
    private MouseSpawner defenderSpawner;
    private Transform defenders;
    private Transform bullets;
    private GameObject gameArea;
    private AttackerSpawner[] attackerSpawner;
    private HealthHandler healthHandler;
    private Image backGround;

    private bool lvlOver;
    private int lvl;
    private bool isRunning;
    private byte maxColorValue;
    private byte minColorValue;

    public bool LvlOver { get => lvlOver; set => lvlOver = value; }

    private void Awake()
    {
        lvlOver = false;
        lvl = 1;
        isRunning = false;
        maxColorValue = 255;
        minColorValue = 150;
        CacheParameters();
    }

    #region CacheParameters
    private void CacheParameters()
    {
        gameArea = GameObject.FindGameObjectWithTag(GlobalVar.GAME_AREA_TAG);
        attackerSpawner = FindObjectsOfType<AttackerSpawner>();
        healthHandler = FindObjectOfType<HealthHandler>();
        myGameTimer = FindObjectOfType<GameTimer>();
        backGround = GameObject.FindGameObjectWithTag(GlobalVar.BACKGROUND_TAG).GetComponent<Image>();
    }
    #endregion

    private void Update()
    {
        if (lvlOver)
        {
            if (!isRunning)
            {
                StartCoroutine(StartNewLevel());
            }
        }
    }

    private IEnumerator StartNewLevel()
    {
        isRunning = true;
        PlayWinAnimation();
        StartCoroutine(SetBackGroundColor());
        yield return new WaitForSeconds(canvasAnimationDuration);
        isRunning = false;
        ResetLevelParameters();
    }

    private IEnumerator SetBackGroundColor()
    {
        yield return new WaitForSeconds(canvasAnimationDuration / 2.0f);
        var randomColorValue = (byte)UnityEngine.Random.Range(minColorValue, maxColorValue);
        var randomColor = new Color32(randomColorValue, randomColorValue, randomColorValue, maxColorValue);
        backGround.color = randomColor;
    }

    // TODO Reset stars
    #region CoroutineFunctionsCall
    private void ResetLevelParameters()
    {
        lvlOver = false;
        UpdateLifesBasedOnLevel();
        EnableBackSpawners();
        PlayFadeOutAnimations();
        EnableClokOnGameArea();
        EnableGameTimer();
    }

    private void PlayWinAnimation()
    {
        ClearDefenderFromField();
        ClearBulletFromField();
        DisableClickOnGameArea();
        AudioSource.PlayClipAtPoint(audioClip, this.transform.position);
        PlayFadeInAnimations();
        IncrementLevel();
        DisableGameTimer();
    }
    #endregion

    #region EnableFunctions
    private void EnableGameTimer()
    {
        myGameTimer.LevelTime = Time.timeSinceLevelLoad + (myGameTimer.LevelTime * lvl);
        myGameTimer.SetSliderPositionToZero(Time.timeSinceLevelLoad, myGameTimer.LevelTime);
        myGameTimer.enabled = true;
    }

    private void EnableClokOnGameArea()
    {
        gameArea.SetActive(true);
    }

    private void EnableBackSpawners()
    {
        foreach (AttackerSpawner spawner in attackerSpawner)
        {
            spawner.Spawn = true;
        }
    }
    #endregion

    #region PlayAnimations
    private void PlayFadeOutAnimations()
    {
        defendersUIAnimator.SetTrigger(GlobalVar.DEFENDERS_UI_FADE_IN_TRIGGER);
        sliderAnimator.SetTrigger(GlobalVar.SLIDER_SLIDE_IN_TRIGGER);
    }

    private void PlayFadeInAnimations()
    {
        defendersUIAnimator.SetTrigger(GlobalVar.DEFENDERS_UI_FADE_OUT_TRIGGER);
        sliderAnimator.SetTrigger(GlobalVar.SLIDER_SLIDE_OUT_TRIGGER);
        winCanvasAnimator.SetTrigger(GlobalVar.WIN_SCREEN_TRIGGER);
    }
    #endregion

    #region UpdateParameters
    private void UpdateLifesBasedOnLevel()
    {
        healthHandler.HealthPoints += lvl;
        healthHandler.UpdateHealthText();
    }

    private void IncrementLevel()
    {
        ++lvl;
    }
    #endregion

    #region DisableFunctions
    private void DisableGameTimer()
    {
        myGameTimer.enabled = false;
    }


    private void DisableClickOnGameArea()
    {
        gameArea.SetActive(false);
    }
    #endregion

    #region ClearField
    public void ClearBulletFromField()
    {
        bullets = GameObject.FindGameObjectWithTag(GlobalVar.BULLETS_POOL_TAG).transform;
        foreach (Transform child in bullets)
        {
            child.GetComponent<Bullet>().DestroyOnDemand();
        }
    }

    public void ClearDefenderFromField()
    {
        defenderSpawner = FindObjectOfType<MouseSpawner>();
        defenders = defenderSpawner.transform;
        foreach (Transform child in defenders)
        {
            defenderSpawner.RemovePositionFromGridMapWahenDestroyed(child.position);
            Destroy(child.gameObject);
        }
    }
    #endregion
}
