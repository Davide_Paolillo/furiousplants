﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class PrefsVariables
{
    private static float volume = 1.0f;
    private static int difficulty = 1;

    private static readonly string VOLUME = "Volume";
    private static readonly string DIFFICULTY = "Difficulty";

    public static int Difficulty { get => difficulty; set => difficulty = value; }
    public static float Volume { get => volume; set => volume = value; }

    public static string VOLUME_SETTING => VOLUME;

    public static string DIFFICULTY_SETTING => DIFFICULTY;

    public static float GetVolume { get => volume; }

    public static float GetDifficulty { get => difficulty; }
}
