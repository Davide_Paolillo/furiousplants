﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class FirstLevelLoader : MonoBehaviour
{
    [SerializeField] private float loadDelay = 3.0f;

    private SceneLoader sceneLoader;

    void Start()
    {
        sceneLoader = GameObject.FindObjectOfType<SceneLoader>();
        sceneLoader.LoadNextSceneDelayed(loadDelay);
    }
}
