﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public static class DifficultyManager
{
    public static int SetADBasedOnDifficulty (int attackDmg)
    {
        if (PlayerPrefs.HasKey(PrefsVariables.DIFFICULTY_SETTING))
        {
            return (attackDmg * PrefsVariables.Difficulty);
        }
        else
        {
            return attackDmg;
        }
    }
}
