﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;
using UnityEngine.UI;

public class SetNewOptions : MonoBehaviour
{
    [SerializeField] Slider volumeSlider;
    [SerializeField] Slider difficultySlider;

    private void Start()
    {
        if (PlayerPrefs.HasKey(PrefsVariables.VOLUME_SETTING))
        {
            volumeSlider.value = PlayerPrefs.GetFloat(PrefsVariables.VOLUME_SETTING);
        }

        if (PlayerPrefs.HasKey(PrefsVariables.DIFFICULTY_SETTING))
        {
            difficultySlider.value = PlayerPrefs.GetInt(PrefsVariables.DIFFICULTY_SETTING);
        }
    }

    public void ModifyVolumeValue()
    {
        PrefsVariables.Volume = volumeSlider.value;
        PlayerPrefs.SetFloat(PrefsVariables.VOLUME_SETTING, PrefsVariables.Volume);
    }

    public void ModifyDifficultyValue()
    {
        PrefsVariables.Difficulty = Mathf.RoundToInt(difficultySlider.value);
        PlayerPrefs.SetInt(PrefsVariables.DIFFICULTY_SETTING, PrefsVariables.Difficulty);
    }

    public void SavePrefs()
    {
        PlayerPrefs.Save();
    }
}
