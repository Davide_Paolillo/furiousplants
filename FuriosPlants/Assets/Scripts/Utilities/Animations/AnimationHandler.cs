﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationHandler : AnimationCoroutines
{
    protected void StartAttackCoroutine(GameObject objToAttack, GameObject enemyType)
    {
        if (enemyType.gameObject.tag.Equals(GlobalVar.LIZARD_TAG))
        {
            StartCoroutine(AttackDefender(objToAttack));
        }
        else if (attacker.gameObject.tag.Equals(GlobalVar.ENEMY_TAG))
        {
            StartCoroutine(FoxAttackDefender(objToAttack));
        }
    }

    protected void StartWalkDelayedCoroutine()
    {
        StartCoroutine(StartWalkDelayed());
    }

    protected void StartWalkImmediate()
    {
        KeepWalk();
    }

    protected virtual void JumpObstacles() { }

    protected virtual void PassTroughUnits() { }
}
