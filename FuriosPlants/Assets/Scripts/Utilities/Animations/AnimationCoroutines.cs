﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationCoroutines : AnimationParameterHandler
{
    protected IEnumerator AttackDefender(GameObject otherObj)
    {
        while (otherObj != null)
        {
            Attack(otherObj);
            yield return new WaitForSeconds(timeToWaitBetweenAttacks);
            StopAttack();
        }
        yield return null;
    }

    protected IEnumerator FoxAttackDefender(GameObject otherObj)
    {
        var loop = true;
        do
        {
            if (otherObj != null && otherObj.GetComponent<Defender>().Health <= attacker.AttackDamage)
            {
                Attack(otherObj);
                loop = false;
                yield return StartWalkDelayed();
            }
            else if (otherObj != null)
            {
                Attack(otherObj);
                yield return new WaitForSeconds(timeToWaitBetweenAttacks);
                StopAttack();
            }
        } while (loop);
        yield return null;
    }

    protected IEnumerator StartWalkDelayed()
    {
        yield return new WaitForSeconds(0.5f);
        KeepWalk();
    }
}
