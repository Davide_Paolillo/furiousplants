﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class AnimationParameterHandler : MonoBehaviour
{
    protected Animator animator;
    protected Attacker attacker;

    protected float oneSecond;
    protected float timeToWaitBetweenAttacks;

    private void Start()
    {
        InitializeParameters();
    }

    private void InitializeParameters()
    {
        oneSecond = 1.0f;
        animator = GetComponent<Animator>();
        attacker = GetComponent<Attacker>();
        timeToWaitBetweenAttacks = oneSecond / attacker.AttacksPerSecond;
    }

    protected void StopAttack()
    {
        animator.SetBool(GlobalVar.IS_ATTACKING_ANIMATION, false);
    }

    protected void KeepWalk()
    {
        animator.SetBool(GlobalVar.IS_ATTACKING_ANIMATION, false);
        animator.SetBool(GlobalVar.CAN_WALK_ANIMATION, true);
    }

    protected void Attack(GameObject target)
    {
        if (target.GetComponent<Defender>())
        {
            animator.SetBool(GlobalVar.IS_ATTACKING_ANIMATION, true);
            animator.SetBool(GlobalVar.CAN_WALK_ANIMATION, false);
            var damageDealt = -attacker.AttackDamage;
            target.GetComponent<Defender>().UpdateHealth(damageDealt);
        }
    }
}
