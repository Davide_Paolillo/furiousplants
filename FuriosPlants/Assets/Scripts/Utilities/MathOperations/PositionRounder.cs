﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class PositionRounder
{
    private float yPadding = 0.2f;
    private float xPadding = 0.2f;

    public Vector3 RoundedPosition(Vector3 vect)
    {
        var newX = Mathf.RoundToInt(vect.x + xPadding);
        var newY = Mathf.RoundToInt(vect.y + yPadding);
        var newZ = Mathf.RoundToInt(vect.z);
        return new Vector3(newX, newY, newZ);
    }

    public Vector3 RoundedYPosition(Vector3 vect)
    {
        var newY = Mathf.RoundToInt(vect.y + yPadding);
        return new Vector3(vect.x, newY, vect.z);
    }
}
