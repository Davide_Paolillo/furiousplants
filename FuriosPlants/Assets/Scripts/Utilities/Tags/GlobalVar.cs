﻿using TagsHandler;

public static class GlobalVar
{
    public static string LIZARD_TAG { get => EnemyTags.LIZARD; }
    public static string PROJECTILE_TAG { get => ProjectileTags.PROJECTILE; }
    public static string GAME_AREA_TAG { get => ZoneTags.GAME_AREA; }
    public static string PROJECTILE_COLLIDER_TAG { get => ProjectileTags.PROJECTILE_COLLIDER; }
    public static string DEFENDER_TAG { get => DefenderTags.DEFENDER; }
    public static string RESOURCES_TEXT_TAG { get => PlayerStatisticsTags.RESOURCES_TEXT; }
    public static string SPAWNER_TAG { get => SpawnerTags.SPAWNER; }
    public static string IS_ATTACKING_ANIMATION { get => AnimationConditions.IS_ATTACKING; }
    public static string ADD_ANIMATION { get => PlayerStatisticsTags.ADD; }
    public static string SUBTRACT_ANIMATION { get => PlayerStatisticsTags.SUBTRACT; }
    public static string IS_GETTING_DAMAGE_ANIMATION { get => AnimationConditions.IS_GETTING_DAMAGE; }
    public static string CAN_WALK_ANIMATION { get => AnimationConditions.CAN_WALK; }
    public static string GRAVESTONE_TAG { get => DefenderTags.GRAVESTONE; }
    public static string JUMP_TRIGGER { get => AnimationConditions.JUMP; }
    public static string ENEMY_TAG { get => EnemyTags.ENEMY; }
    public static string DEAD_ZONE_TAG { get => ZoneTags.DEAD_ZONE; }
    public static string IS_HEALTH_CHANGED_ANIMATION { get => AnimationConditions.IS_HEALTH_CHANGED; }
    public static string WIN_SCREEN_TRIGGER { get => AnimationConditions.WIN_SCREEN; }
    public static string DEFENDERS_UI_FADE_OUT_TRIGGER { get => AnimationConditions.DEFENDERS_UI_FADE_OUT; }
    public static string DEFENDERS_UI_FADE_IN_TRIGGER { get => AnimationConditions.DEFENDERS_UI_FADE_IN; }
    public static string SLIDER_SLIDE_OUT_TRIGGER { get => AnimationConditions.SLIDER_SLIDE_OUT; }
    public static string SLIDER_SLIDE_IN_TRIGGER { get => AnimationConditions.SLIDER_SLIDE_IN; }
    public static string LOSE_TRIGGER { get => AnimationConditions.LOSE; }
    public static string BULLETS_POOL_TAG { get => ProjectileTags.BULLETS_POOL; }
    public static string BACKGROUND_TAG { get => ZoneTags.BACKGROUND; }
    public static string MENU_SCENE { get => SceneNames.MENU; }
    public static string GAME_SCENE { get => SceneNames.GAME; }
    public static string OPTIONS_SCENE { get => SceneNames.OPTIONS; }
}
