﻿namespace TagsHandler
{
    public readonly struct SceneNames
    {
        public static readonly string MENU = "StartScene";
        public static readonly string GAME = "LevelOne";
        public static readonly string OPTIONS = "Options";
    }

    public readonly struct DefenderTags
    {
        public static readonly string DEFENDER = "Defender";
        public static readonly string GRAVESTONE = "Gravestone";
    }

    public readonly struct EnemyTags
    {
        public static readonly string LIZARD = "Lizard";
        public static readonly string ENEMY = "Enemy";
    }

    public readonly struct ProjectileTags
    {
        public static readonly string PROJECTILE = "Projectile";
        public static readonly string PROJECTILE_COLLIDER = "ProjectileCollider";
        public static readonly string BULLETS_POOL = "BulletsPool";
    }

    public readonly struct ZoneTags
    {
        public static readonly string DEAD_ZONE = "DeadZone";
        public static readonly string GAME_AREA = "GameArea";
        public static readonly string BACKGROUND = "BackGround";
    }

    public readonly struct SpawnerTags
    {
        public static readonly string SPAWNER = "Spawner";
    }

    public readonly struct PlayerStatisticsTags
    {
        public static readonly string RESOURCES_TEXT = "Resources";
        public static readonly string ADD = "add";
        public static readonly string SUBTRACT = "subtract";
    }

    public readonly struct AnimationConditions
    {
        public static readonly string IS_ATTACKING = "IsAttacking";
        public static readonly string IS_GETTING_DAMAGE = "IsGettingDamage";
        public static readonly string CAN_WALK = "CanWalk";
        public static readonly string JUMP = "JumpTrigger";
        public static readonly string IS_HEALTH_CHANGED = "IsHealthChanged";
        public static readonly string WIN_SCREEN = "WinScreen";
        public static readonly string DEFENDERS_UI_FADE_OUT = "FadeOut";
        public static readonly string DEFENDERS_UI_FADE_IN = "FadeIn";
        public static readonly string SLIDER_SLIDE_OUT = "SlideOut";
        public static readonly string SLIDER_SLIDE_IN = "SlideIn";
        public static readonly string LOSE = "Lose";
    }

}
