﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class HealthHandler : MonoBehaviour
{
    [Header("Stats")]
    [SerializeField] private int HP = 5;

    [Header("Animators")]
    [SerializeField] private Animator defendersUIAnimator;
    [SerializeField] private Animator sliderAnimator;
    [SerializeField] private Animator loseCanvas;
    [SerializeField] private Animator smoothTransition;

    [Header("Loaders")]
    [SerializeField] private LevelLoader levelLoader;
    [SerializeField] private SceneLoader sceneLoader;

    private TextMeshProUGUI hpText;
    private Animator animator;

    private int prevHP;
    private bool isRunning;

    public int HealthPoints { get => HP; set => HP = value; }

    void Start()
    {
        isRunning = false;
        animator = GetComponent<Animator>();
        hpText = GetComponent<TextMeshProUGUI>();
        hpText.text = HP.ToString();
        prevHP = HP;
    }

    void Update()
    {
        StartTextAnimation();
        CheckLose();
    }

    private void StartTextAnimation()
    {
        if (HP < prevHP && HP >= 0)
        {
            StartCoroutine(TextAnimation());
            prevHP = HP;
        }
    }

    private void CheckLose()
    {
        if (HP <= 0)
        {
            levelLoader.ClearBulletFromField();
            levelLoader.ClearDefenderFromField();
            ClearEnemiesFromField();
            if (!isRunning)
            {
                isRunning = true;
                StartCoroutine(PlayAnimations());
            }
        }
    }

    private void ClearEnemiesFromField()
    {
        var enemies = FindObjectsOfType<Attacker>();
        foreach (Attacker enemy in enemies)
        {
            Destroy(enemy.gameObject);
        }
    }

    #region UpdateHealthFuntions
    public void UpdateHealthText()
    {
        hpText.text = HP.ToString();
        if (HP > prevHP)
        {
            prevHP = HP;
        }
    }

    public void UpdateHealth(GameObject enemy)
    {
        if (enemy.tag.Equals(GlobalVar.ENEMY_TAG) || enemy.tag.Equals(GlobalVar.LIZARD_TAG))
        {
            HP -= 1;
        }
    }
    #endregion

    #region CoroutinesImplementation
    private IEnumerator PlayAnimations()
    {
        defendersUIAnimator.SetTrigger(GlobalVar.DEFENDERS_UI_FADE_OUT_TRIGGER);
        sliderAnimator.SetTrigger(GlobalVar.SLIDER_SLIDE_OUT_TRIGGER);
        loseCanvas.SetTrigger(GlobalVar.LOSE_TRIGGER);
        yield return new WaitForSeconds(1.0f);
        sceneLoader.LoadNextScene();
    }

    private IEnumerator TextAnimation()
    {
        animator.SetBool(GlobalVar.IS_HEALTH_CHANGED_ANIMATION, true);
        yield return new WaitForSeconds(1.0f);
        animator.SetBool(GlobalVar.IS_HEALTH_CHANGED_ANIMATION, false);
    }
    #endregion
}
