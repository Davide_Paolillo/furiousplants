﻿using System;
using System.Collections;
using System.Collections.Generic;
using TMPro;
using UnityEngine;

public class ResourcesHandler : MonoBehaviour
{
    [SerializeField] private int resources = 1000;

    private TextMeshProUGUI resourcesText;
    private Animator animator;

    private int prevResources;
    private float timeToStopAnimation;

    public int Resources { get => resources; }

    void Start()
    {
        timeToStopAnimation = Mathf.Epsilon;
        resources = 1000;
        prevResources = resources;
        animator = GetComponent<Animator>();
        resourcesText = GameObject.FindGameObjectWithTag(GlobalVar.RESOURCES_TEXT_TAG).GetComponent<TextMeshProUGUI>();
        UpdateResourcesText();
    }

    void Update()
    {
        PlayAnimationOnResourcesChange();
    }

    private void PlayAnimationOnResourcesChange()
    {
        if (resources > prevResources)
        {
            StartCoroutine(PlayAddAnimation());
        }
        else if (resources < prevResources)
        {
            StartCoroutine(PlaySubtractAnimation());
        }
        prevResources = resources;
    }

    private IEnumerator PlaySubtractAnimation()
    {
        animator.SetBool(GlobalVar.SUBTRACT_ANIMATION, true);
        yield return new WaitForSeconds(timeToStopAnimation);
        animator.SetBool(GlobalVar.SUBTRACT_ANIMATION, false);
    }

    private IEnumerator PlayAddAnimation()
    {
        animator.SetBool(GlobalVar.ADD_ANIMATION, true);
        yield return new WaitForSeconds(timeToStopAnimation);
        animator.SetBool(GlobalVar.ADD_ANIMATION, false);
    }

    public void UpdateResourcesText()
    {
        resourcesText.text = resources.ToString();
    }

    public void UpdateResources(int addend)
    {
        this.resources += addend;
    }
}
