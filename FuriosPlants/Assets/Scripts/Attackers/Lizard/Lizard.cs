﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Lizard : AnimationHandler
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        GameObject otherObj = collision.gameObject;
        StartAttackCoroutine(otherObj, this.gameObject);
    }

    private void OnTriggerExit2D(Collider2D collision)
    {
        StartWalkDelayedCoroutine();
    }
}
