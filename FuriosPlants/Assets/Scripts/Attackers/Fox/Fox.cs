﻿using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Fox : AnimationHandler
{
    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.GRAVESTONE_TAG))
        {
            JumpObstacles();
            StartWalkImmediate();
        }
        else if (collision.gameObject.tag.Equals(GlobalVar.DEFENDER_TAG))
        {
            StartAttackCoroutine(collision.gameObject, this.gameObject);
        }
    }

    protected override void JumpObstacles()
    {
        base.JumpObstacles();
        animator = GetComponent<Animator>();
        animator.SetTrigger(GlobalVar.JUMP_TRIGGER);
    }
}
