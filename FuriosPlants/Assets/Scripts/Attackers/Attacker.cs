﻿using System;
using System.Collections;
using System.Collections.Generic;
using UnityEngine;

public class Attacker : MonoBehaviour
{
    [Header("Stats")]
    [Range(0.0f, 5.0f)][SerializeField] private float currentSpeed = 1.0f;
    [SerializeField] private float health = 500.0f;
    [SerializeField] private int attackDamage = 100;
    [SerializeField] private float attacksPerSecond = 0.5f;

    [Header("VFX")]
    [SerializeField] private GameObject deathVFX;

    private Animator animator;
    private GameObject currentTarget;

    public float AttacksPerSecond { get => attacksPerSecond; }
    public int AttackDamage { get => attackDamage; set => attackDamage = value; }

    private void Start()
    {
        animator = GetComponent<Animator>();
    }

    void Update()
    {
        CheckDeadCondition();
        SetMovementSpeed(currentSpeed);
    }

    private void CheckDeadCondition()
    {
        if (health <= 0)
        {
            Vector2 particlePos = new Vector2(this.transform.position.x - 0.8f, this.transform.position.y - 0.2f);
            GameObject particles = Instantiate(deathVFX, particlePos, this.transform.rotation) as GameObject;
            Destroy(particles, 1.0f);
            Destroy(this.gameObject);
        }
    }

    public void SetMovementSpeed(float speed)
    {
        currentSpeed = speed;
        this.transform.Translate(Vector2.left * Time.deltaTime * speed);
    }

    private void OnTriggerEnter2D(Collider2D collision)
    {
        if (collision.gameObject.tag.Equals(GlobalVar.PROJECTILE_TAG))
        {
            float dmgRecieved = collision.gameObject.GetComponent<DamageDealer>().DamageDealt;
            health -= dmgRecieved;
        }
        else if (collision.gameObject.tag.Equals(GlobalVar.DEAD_ZONE_TAG))
        {
            GameObject.FindObjectOfType<HealthHandler>().UpdateHealth(this.gameObject);
            Destroy(this.gameObject);
        }
    }
}
